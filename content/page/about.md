---
title: About InfoSecBudo
subtitle: Passionate InfoSec practice.
comments: false
---

## Budo
Budo, is a term coined from the Japanese, meaning the "martial way", i.e. way of war.  This page is dedicated to the various disciplines and practices of information security.  This is a site for learning, exploring, practice and preparation.  Please practice the **Bushido** (Code of Ethics Cannons).  

**Disciplines covered but not limited:**

- Open Source Intelligence (OSINT)
- Social Engineering
- Defensive tools
- Offensive tools
- Techniques, Tactics and Procedures (TTPs)
- Coding / Scripting
- Policies and Procedures
- Risk Management
- Defense in Depth
- Burn Out

### My History

Over 20 years of experience in the IT industry.  Over 10 years of dedication to the InfoSec practice.  Life long learner.  Background in healthcare enterprise IT and small business IT solutions.  